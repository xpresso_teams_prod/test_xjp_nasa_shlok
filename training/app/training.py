import os
import sys
import types
import keras
import pickle
import marshal
import autofeat
import matplotlib
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from xgboost import XGBRegressor
from keras.models import Sequential
from sklearn.impute import SimpleImputer
from keras.layers import Dense, Dropout, LSTM
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from autofeat import FeatureSelector, AutoFeatRegressor
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from sklearn.preprocessing import MinMaxScaler, RobustScaler, QuantileTransformer
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    train_file = pickle.load(open(f"{PICKLE_PATH}/train_file.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


try:
    feature_engineering = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/feature_engineering.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    data_extraction = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/data_extraction.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    data_cleaning = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/data_cleaning.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))

## $xpr_param_component_name = training
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["train_file"]
## $xpr_param_global_methods = ["feature_engineering", "data_extraction", "data_cleaning"]
# def ML_model(X_train, y_train, X_test, y_test):
#     reg = GridSearchCV(RandomForestRegressor(), {'max_depth':[15], 'n_estimators':[20], 'criterion':['mse'], 'random_state':[0]}, cv=5, scoring = 'r2')
    
#     model_ML = reg.fit(X_train, np.ravel(y_train))
#     print("\nBest parameters from GridSearch = " + str(model_ML.best_params_))
#     print("\nAverage R2 Score with 5-fold CV = " + str(model_ML.best_score_))
#     y_test_pred = model_ML.predict(X_test)
#     R2_test_ML = r2_score(y_test_pred, y_test)
#     print("R2 Score on the test dataset with Random Forest = " + str(R2_test_ML))
#     print("\n")
#     return {model_ML, R2_test_ML}
def LSTM_model(X_train, y_train, X_test, y_test):
    # Normalization
    scaler = QuantileTransformer(n_quantiles=10, output_distribution='normal')
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    X_train_lstm = np.reshape(X_train,(X_train.shape[0], 1, X_train.shape[1])); y_train_lstm = y_train   
    X_test_lstm = np.reshape(X_test,(X_test.shape[0], 1, X_train.shape[1])); y_test_lstm = y_test 

    # Defining a 2-layer stacked LSTM
    model = Sequential()
    model.add(LSTM(32, activation='relu', input_shape=(1, X_train.shape[1]), return_sequences = True))
    model.add(LSTM(32, activation='relu', input_shape=(1, X_train.shape[1])))
    model.add(Dense(1))
    model.compile(optimizer='adam', loss='mse')
    model.fit(X_train_lstm, y_train_lstm, epochs=20, validation_split=0.20)
    y_test_pred_lstm = model.predict(X_test_lstm)
    R2_test_lstm = r2_score(y_test_pred_lstm, y_test_lstm)
    print("R2 Score on the test dataset with LSTM = " + str(R2_test_lstm))
#     if not os.path.exists(self.OUTPUT_DIR):
#             os.makedirs(self.OUTPUT_DIR)
#     model.save(os.path.join(self.OUTPUT_DIR, 'saved_model.h5'))
#     logging.info("Saved model... load model")
#     self.report_status(status={"status": {"status": "Training completed"}})
#     super().completed(push_exp=True)
    #model.save("model_lstm")
    return {model, R2_test_lstm}
# Extracting data
df_initial = data_extraction(train_file)
#Removing columns with NaN and adding the RUL column
df_cleaned = data_cleaning(df_initial)
df_cleaned.head()
# Train-test split, normalization and feature engineering
X_train_transformed, X_test_transformed, y_train, y_test = feature_engineering(df_cleaned)
# Printing transformed dataset after feature engineering 
X_train_transformed.head()
# # # Fitting Random Forest model
# print("1. Using Random Forest:\n")
# model_ML, R2_test_ML = ML_model(X_train_transformed, y_train, X_test_transformed, y_test)

# Fitting LSTM model
print("2. Using LSTM:\n")
model_lstm, R2_test_lstm = LSTM_model(X_train_transformed, y_train, X_test_transformed, y_test)

try:
    pickle.dump(marshal.dumps(feature_engineering.__code__), open(f"{PICKLE_PATH}/feature_engineering.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(marshal.dumps(data_extraction.__code__), open(f"{PICKLE_PATH}/data_extraction.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(marshal.dumps(data_cleaning.__code__), open(f"{PICKLE_PATH}/data_cleaning.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


try:
    pickle.dump(train_file, open(f"{PICKLE_PATH}/train_file.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

